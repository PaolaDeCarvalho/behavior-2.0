import React from 'react';
import {Logo} from './../components/Logo';
import {Logout, AddProject} from './../components/Buttons';

export class Header extends React.Component {
    render() {
        return (
            <header className="header">
                <Logo/>
                <AddProject />
                <Logout/>
            </header>
        );
    }
  }

