import React from 'react';
import {Card, CardActionArea,CardActions, CardContent, CardMedia,Typography, Chip, Dialog, DialogTitle, DialogContent, Button}  from '@material-ui/core/';
import {RenewProject, CompleteProject,AddTodayInfo} from './Buttons'

export class ProjectCard extends React.Component {
    constructor(props){
        super(props);
        //Fazer a busca dos detalhes do projeto pelo ID
        this.state= {
            idProject: props.idProject,
            titulo: "Projeto " + props.idProject,
            meta: "Meta " + props.idProject,
            pontos: 101,
            dataConclusao: new Date('2020,01,03'),
            openRenew: false,
            openComplete: false
        }
    }
    colorPoints(pontos){
        if(pontos < 0) 
            return "primary";
        else if(pontos > 100) 
            return "secondary";        
        else
            return "default";
    }
    handleClickComplete = () => {
        
        this.setState({
            openComplete: !this.state.openComplete
        });
    }
    handleClickRenew = () => {
        this.setState({
            openRenew: !this.state.openRenew
        });
    }
    render() {
        return (
            <div>
                <Card>
                    <CardActionArea>
                        <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                            {this.state.titulo}
                        </Typography>
                        <Chip label={this.state.pontos} variant="outlined" color={this.colorPoints(this.state.pontos)} />
                        <Typography variant="body2" color="textSecondary" component="p">
                            META: {this.state.meta}
                        </Typography>
                        </CardContent>
                    </CardActionArea>
                    <CardActions>
                        {this.state.dataConclusao <= new Date() ? (<div><CompleteProject onClick={this.handleClickComplete}/> <RenewProject onClick={this.handleClickRenew} open={this.state.openRenew}/></div>) : <AddTodayInfo idProject={this.state.idProject}/> }
                    </CardActions>
                </Card>
                <RenewDialog  open={this.state.openRenew} onClose={this.handleClickRenew}/>
                <CompleteDialog  open={this.state.openComplete} onClose={this.handleClickComplete}/>
            </div>
        );
    }
  }

  function RenewDialog(props) {
    const { onClose, selectedValue, open } = props;

    const handleClose = () => {
      onClose(selectedValue);
    };
    return(
        <Dialog open={props.open} onClose={handleClose}>
            <DialogTitle id="alert-dialog-title">{"Renew"}</DialogTitle>
            <DialogContent>
                <Button onClick={handleClose} color="primary">
                    Disagree
                </Button>
            </DialogContent>
            
        </Dialog>
    )
  }

  function CompleteDialog(props){
    const { onClose, selectedValue } = props;

    const handleClose = () => {
      onClose(selectedValue);
    };
    return(
        <Dialog open={props.open} onClose={handleClose}>
            <DialogTitle id="alert-dialog-title">{"Complete"}</DialogTitle>
            <DialogContent>
                <Button onClick={handleClose} color="primary">
                    Disagree
                </Button>
            </DialogContent>
            
        </Dialog>
    )
  }