import React from 'react';
import {Header} from './../components/Header';
import {Footer} from './../components/Footer';
import {Container, Grid, Fade, Collapse, Grow} from '@material-ui/core/';
import { ProjectCard } from '../components/Project';

let time = 500;
export default class Inside extends React.Component {
    constructor(props){
        super(props);
    }
    timeout(){
        time = time + 500;
        return time;
    }
    render() {
        //Fazer a busca de projetos do usuário logado
        const projects = [1,2,3,4,5,6];

        return (
            <div>
                <Header/>
                <main>
                    <Grid container  spacing={2}>
                        {projects.map((value, index) => {
                            return (
                                <Grow key={index} in={true} timeout={this.timeout()}>
                                    <Grid item xs={3}>
                                        <ProjectCard idProject={value} />
                                    </Grid>
                                </Grow>
                            )
                        })}
                    </Grid>
                </main>
                <Footer/>
            </div>
        );
    }
  }

