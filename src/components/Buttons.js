import React from 'react';
import {Icon, IconButton, Tooltip} from '@material-ui/core/';

export class Logout extends React.Component {
    logout() {
        window.location.href = "/";
    }
    render() {
        return (
            <Tooltip title="Sair" enterDelay={200} leaveDelay={200}>
                <IconButton aria-label="delete" onClick={this.logout}>
                    <Icon className="fas fa-sign-out-alt"></Icon>
                </IconButton>
            </Tooltip>
        );
    }
}

export class AddProject extends React.Component {
    add() {
        window.location.href = "/new";
    }
    render() {
        return (
            <Tooltip title="Novo Projeto" enterDelay={200} leaveDelay={200}>
                <IconButton aria-label="add" onClick={this.add}>
                    <Icon className="fas fa-plus"></Icon>
                </IconButton>
            </Tooltip>
        );
    }
}

export class CompleteProject extends React.Component {
    render() {
        return (
            <Tooltip title="Completar Projeto" enterDelay={200} leaveDelay={200}>
                <IconButton aria-label="complete" onClick={this.props.onClick}>
                    <Icon className="far fa-calendar-check"></Icon>
                </IconButton>
            </Tooltip>
        );
    }
}

export class AddTodayInfo extends React.Component {
    addInfo() {
        window.location.href = "/today";
    }
    render() {
        return (
            <Tooltip title="Incluir dados de hoje" enterDelay={200} leaveDelay={200}>
                <IconButton aria-label="info" onClick={this.addInfo}>
                    <Icon className="far fa-calendar-plus"></Icon>
                </IconButton>
            </Tooltip>
        );
    }
}

export class RenewProject extends React.Component {
    render() {
        return (
            <Tooltip title="Renovar Projeto" enterDelay={200} leaveDelay={200}>
                <IconButton aria-label="renew" onClick={this.props.onClick}>
                    <Icon className="fas fa-sync"></Icon>
                </IconButton>
            </Tooltip>
        );
    }
}


