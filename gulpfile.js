const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require("gulp-autoprefixer");
const rename = require("gulp-rename");


function compileSass(){
    return gulp
    .src('./src/main.scss')
    .pipe(sass({outputStyle: "compressed"}).on("error", sass.logError))
    .pipe(
        autoprefixer({
            overrideBrowserslist: ["last 2 versions"],
            cascade: false
        })
    )
    .pipe(rename({ basename: "index" }))
    .pipe(gulp.dest('src/'))
}

function watch() {
    gulp.watch('./src/**/*.scss',gulp.series('sass'));
}

gulp.task('sass', compileSass);
gulp.task('w', watch);