import React from 'react';
import {Login as LoginForm} from '../components/Forms';
import {Logo} from '../components/Logo';
import {Container, Grid, Slide, Box } from '@material-ui/core/';


export default class Login extends React.Component {
    render() {
        return (
            <Container maxWidth="sm">
                <Grid container justify="center">
                    <Slide direction="up" in={true} mountOnEnter unmountOnExit className="formAutenticacao">
                        <Box boxShadow={3} width={1}>
                            <Logo maxWidth="sm" center="true"/>
                            <LoginForm/>
                        </Box>
                    </Slide>
                </Grid>
            </Container>
        );
    }
  }

