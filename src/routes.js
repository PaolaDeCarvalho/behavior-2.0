import React from "react";
import {
  BrowserRouter,
  Switch,
  Route,
  Link,
  useHistory
} from "react-router-dom";
import Login from './pages/login';
import Inside from './pages/inside';
import NewProject from './pages/new';

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path="/" component={Login} />
            <Route path="/home" component={Inside} />
            <Route path="/new" component={NewProject} />
        </Switch>
    </BrowserRouter>
);

export default Routes;