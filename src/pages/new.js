import React from 'react';
import {Header} from './../components/Header';
import {Footer} from './../components/Footer';
import {NewProject as NewProjectForm} from './../components/Forms';
import {Container, Grid, Fade, Collapse, Grow} from '@material-ui/core/';
import { ProjectCard } from '../components/Project';

let time = 500;
export default class NewProject extends React.Component {
    constructor(props){
        super(props);
    }
    render() {

        return (
            <div>
                <Header/>
                <main>
                    <Grid container  spacing={2}>
                        <NewProjectForm/>
                    </Grid>
                </main>
                <Footer/>
            </div>
        );
    }
  }

