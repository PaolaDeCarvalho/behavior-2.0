import React from 'react';
import Routes from './routes';
import { loadCSS } from 'fg-loadcss';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';

const behaviorTheme = createMuiTheme({
  palette: {
    primary: {main: "#f8a5c2", dark: "#f78fb3"},
    secondary: {main: "#55efc4", dark: "#00b894"},
    error: {main: "#f3a683", dark:"#f19066"},
    warning: {main: "#f7d794", dark:"#f5cd79"},
    success: {main: "#00b894", light: "#55efc4"},
    info: {main: "#63cdda", dark:"#3dc1d3"},
    default: {main: "#3dc1d3"},
  },
});

function App() {
  React.useEffect(() => {
    loadCSS(
      'https://use.fontawesome.com/releases/v5.12.0/css/all.css',
      document.querySelector('#font-awesome-css'),
    );
  }, []);
  
  return (
    <ThemeProvider theme={behaviorTheme}>
      <Routes/>
    </ThemeProvider>
  );
}

export default App;
