import React from 'react';
import {Box} from '@material-ui/core/';
import LogoImg from  './../img/logo.png';

export class Logo extends React.Component {
        render() {
            return (
                <Box className="logo">
                    <img src={LogoImg} alt="Behavior" className={this.props.center ? "center" : ""} />
                </Box>
            );
        }
  }