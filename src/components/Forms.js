import React from 'react';
import {Input, Button, FormGroup, Step,Stepper,StepLabel, IconButton, Icon} from '@material-ui/core/';

export class Login extends React.Component {
      handleSubmit(event) {
        window.location.href = "/home";
        event.preventDefault();
      }
    
      render() {
        return (
            <form onSubmit={this.handleSubmit} noValidate width={1}>
                <FormGroup>
                    <Input placeholder="Usuário"  color="secondary" />
                </FormGroup>
                <FormGroup>
                    <Input type="password" placeholder="Senha" width={1}  color="secondary" />
                </FormGroup>
                <Button type="submit" variant="contained" color="primary">
                    Entrar
                </Button>
            </form>
        );
      }
  }

export class NewProject extends React.Component {
    handleSubmit(event) {
      window.location.href = "/home";
      event.preventDefault();
    }
    add(event){
        console.log(event);
    }
  
    render() {
      return (
          <form onSubmit={this.handleSubmit} noValidate width={1}>
            <Stepper activeStep={0} alternativeLabel>
                <Step key={1} >
                    <StepLabel>Criando um projeto</StepLabel>
                    <FormGroup>
                        <Input placeholder="Título" name="titulo" />
                        <Input type="date" placeholder="Data prevista para conclusão" name="dataFim" />
                    </FormGroup>
                    <FormGroup>
                        <Input placeholder="Hábito Bom" name="habitoBom" />
                        <Input placeholder="Hábito Ruim" name="habitoRuim" />
                    </FormGroup>
                    <FormGroup>
                        <Input placeholder="Meta" name="meta" multiline={true} rows={5}/>
                    </FormGroup>
                    <Button type="button"  variant="contained" color="primary">
                        Próximo
                    </Button>
                </Step>
                <Step key={2}>
                    <StepLabel>Adicionando itens às categorias</StepLabel>
                    <FormGroup>
                        <Input placeholder="Item para hábito bom" name="itemHabitoBom" />
                        <Input type="number" placeholder="Pontos (+)" name="pontosHabitoBom" />
                        <IconButton aria-label="add" variant="contained" onClick={this.add}>
                            <Icon className="fas fa-plus"></Icon>
                        </IconButton>
                    </FormGroup>
                    <FormGroup>
                        <Input placeholder="Item para hábito ruim" name="itemHabitoRuim" />
                        <Input type="number" placeholder="Pontos (-)" name="pontosHabitoRuim" />
                        <IconButton aria-label="add" variant="contained" onClick={this.add}>
                            <Icon className="fas fa-plus"></Icon>
                        </IconButton>
                    </FormGroup>
                </Step>
                
              
            </Stepper>
          </form>
      );
    }
}